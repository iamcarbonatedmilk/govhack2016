all: train

train: model/data_tokenized.pkl
	cd model && python train.py

.PHONY: all train

alldata.pkl: merge_data.py $(wildcard tweets/*.csv)
	python merge_data.py

model/data_tokenized.pkl: model/prepare_all_data.py alldata.pkl
	cd model && python prepare_all_data.py
